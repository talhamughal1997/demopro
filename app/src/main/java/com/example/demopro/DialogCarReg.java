package com.example.demopro;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Arrays;

public class DialogCarReg extends DialogFragment implements RecyclerAdapter.itemClickListener {

    private RecyclerView mRecyclerView;
    ImageView mImageView_back;
    DialogCarReg.DialogItemClick mlistener;

    View view;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_car_reg, null, false);
        seImgBack();
        setRecyclerView(getArrayList());
        return new AlertDialog.Builder(getActivity(), R.style.DialogTheme)
                .setView(view)
                .create();
    }

    private void setRecyclerView(ArrayList<String> arrayList) {
        mRecyclerView = view.findViewById(R.id.recyclerview);
        RecyclerAdapter adapter = new RecyclerAdapter(arrayList, this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(adapter);
    }

    private void seImgBack() {
        mImageView_back = view.findViewById(R.id.img_back);
        mImageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private ArrayList<String> getArrayList() {
        String[] stringlist = getActivity().getResources().getStringArray(R.array.cities_list);
        ArrayList<String> arrayList = new ArrayList(Arrays.asList(stringlist));
        return arrayList;
    }

    @Override
    public void onItemClick(String string) {
        mlistener.onItemClick(string);
        dismiss();
    }

    public void setOnDialogItemClick(DialogCarReg.DialogItemClick dialogItemClick) {
        this.mlistener = dialogItemClick;
    }

    interface DialogItemClick {
        void onItemClick(String string);
    }

}
