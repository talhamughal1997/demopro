package com.example.demopro;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.MyViewHolder> {
    private ArrayList<Bitmap> imagesList;
    private Activity activity;
    private ClearArray listener;

    public PhotosAdapter(ArrayList<Bitmap> imagesList, Activity activity) {
        this.imagesList = imagesList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_photos, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.imageView.setImageBitmap(imagesList.get(i));
    }

    @Override
    public int getItemCount() {
        return imagesList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img_photo);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DeleteDialog dialog = new DeleteDialog();
                    dialog.setOnItemClickListener(new DeleteDialog.itemClickListener() {
                        @Override
                        public void itemClick() {
                            imagesList.remove(getAdapterPosition());
                            notifyDataSetChanged();
                            if (imagesList.size()<1){
                                listener.onClearArray();
                            }
                        }
                    });
                    dialog.show(((AppCompatActivity) activity).getSupportFragmentManager(), "deleteDialog");
                }
            });
        }
    }

    public void setOnClearArray(ClearArray listener){
        this.listener = listener;
    }

    interface ClearArray{
        void onClearArray();
    }


}
