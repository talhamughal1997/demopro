package com.example.demopro;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import static com.example.demopro.ChooseDialog.CAMERA_REQUEST;
import static com.example.demopro.ChooseDialog.MY_CAMERA_PERMISSION_CODE;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    CardView mCardView_upload, mCardView_photos;
    RecyclerView recyclerView;
    PhotosAdapter adapter;
    LinearLayout ln_AddPhotos;
    Button btn_Sell;
    EditText edt_Location, edt_carInfo, edt_carReg, edt_bodycolor, edt_mileage;
    ArrayList<Bitmap> imagesList;
    String refreshedToken;
    ProgressDialog progressDialog;
    FirebaseStorage storage;
    StorageReference storageReference;
    private int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewInit();
        viewListners();
        setRecyclerView(imagesList);
        checkPermission(this);
        checkEmptyArray();
    }

    private void viewInit() {
        mCardView_upload = findViewById(R.id.cardview_upload);
        mCardView_photos = findViewById(R.id.cardview_photos);
        ln_AddPhotos = findViewById(R.id.ln_add_photos);
        btn_Sell = findViewById(R.id.btn_sell);
        edt_Location = findViewById(R.id.edt_location);
        edt_carInfo = findViewById(R.id.edt_car_info);
        edt_bodycolor = findViewById(R.id.edt_body_color);
        edt_carReg = findViewById(R.id.edt_reg_city);
        edt_mileage = findViewById(R.id.edt_mil);
        progressDialog = new ProgressDialog(this);
        recyclerView = findViewById(R.id.rec_photos);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        imagesList = new ArrayList<>();
    }

    private void viewListners() {
        mCardView_upload.setOnClickListener(this);
        ln_AddPhotos.setOnClickListener(this);
        btn_Sell.setOnClickListener(this);
        edt_Location.setOnClickListener(this);
        edt_carInfo.setOnClickListener(this);
        edt_bodycolor.setOnClickListener(this);
        edt_carReg.setOnClickListener(this);
    }

    private void setRecyclerView(ArrayList<Bitmap> imagesList) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        adapter = new PhotosAdapter(imagesList, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cardview_upload: {
                if (imagesList.size() < 10) {
                    ChooseDialog dialog = new ChooseDialog();
                    dialog.setCancelable(false);
                    dialog.show(getSupportFragmentManager(), "choose");
                }
                break;
            }
            case R.id.ln_add_photos: {
                if (imagesList.size() < 10) {
                    ChooseDialog dialog = new ChooseDialog();
                    dialog.setCancelable(false);
                    dialog.show(getSupportFragmentManager(), "choose");
                }
                break;
            }
            case R.id.btn_sell: {
                if (!isEmpty()) {
                    savePhoto(imagesList);
                } else {
                    Toast.makeText(this, "Plz Select All Fields", Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case R.id.edt_location: {
                DialogLocation dialog = new DialogLocation();
                dialog.show(getSupportFragmentManager(), "location");
                dialog.setOnDialogItemClick(new DialogLocation.DialogItemClick() {
                    @Override
                    public void onItemClick(String string) {
                        edt_Location.setText(string);
                    }
                });
                break;
            }
            case R.id.edt_car_info: {
                DialogCarInfo dialog = new DialogCarInfo();
                dialog.show(getSupportFragmentManager(), "carinfo");
                dialog.setOnDialogItemClick(new DialogCarInfo.DialogItemClick() {
                    @Override
                    public void onItemClick(String string) {
                        edt_carInfo.setText(string);
                    }
                });
                break;
            }
            case R.id.edt_reg_city: {
                DialogCarReg dialog = new DialogCarReg();
                dialog.show(getSupportFragmentManager(), "regcity");
                dialog.setOnDialogItemClick(new DialogCarReg.DialogItemClick() {
                    @Override
                    public void onItemClick(String string) {
                        edt_carReg.setText(string);
                    }
                });
                break;
            }
            case R.id.edt_body_color: {
                DialogBodyColor dialog = new DialogBodyColor();
                dialog.setCancelable(false);
                dialog.show(getSupportFragmentManager(), "bodycolor");
                dialog.setOnColorClickListener(new DialogBodyColor.itemClick() {
                    @Override
                    public void selectedColor(String string) {
                        edt_bodycolor.setText(string);
                    }
                });
                break;
            }

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void getImagesWithCamera() {
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
        } else {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        }

    }


    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
            count = 0;
            count = imagesList.size();
            for (Image image : images) {
                File file = new File(image.path);
                String filePath = file.getPath();
                Bitmap bitmap = BitmapFactory.decodeFile(filePath);
                imagesList.add(0, bitmap);
                adapter.notifyDataSetChanged();
            }
        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            imagesList.add(0, photo);
            if (imagesList.size() < 10) {
                getImagesWithCamera();
            }
        }
        if (imagesList.size() > 0) {
            mCardView_photos.setVisibility(View.VISIBLE);
            mCardView_upload.setVisibility(View.GONE);
        }
    }

    private void savePhoto(ArrayList<Bitmap> imagesList) {
        progressDialog.setTitle("Uploading...");
        progressDialog.show();
        if (imagesList != null && imagesList.size() == 10) {
            for (int i = 0; i < imagesList.size(); i++) {
                final String pushid = FirebaseDatabase.getInstance().getReference().child("Data").push().getKey();
                Bitmap bitmap = imagesList.get(i);
                StorageReference ref = storageReference.child("images/" + pushid);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 20, baos);
                final byte[] data = baos.toByteArray();
                UploadTask uploadTask = ref.putBytes(data);
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                        final Task<Uri> downUrl = taskSnapshot.getMetadata().getReference().getDownloadUrl();
                        downUrl.addOnCompleteListener(new OnCompleteListener<Uri>() {
                            @Override
                            public void onComplete(@NonNull Task<Uri> task) {
                                if (task.isSuccessful()) {
                                    Log.i("url:", task.getResult().toString());

                                    Uri downloadUrl = task.getResult();
                                    Log.d("downloadUrl-->", "" + downloadUrl);

                                    HashMap<String, String> model = new HashMap<>();
                                    model.put("Location", edt_Location.getText().toString());
                                    model.put("Car_Info", edt_carInfo.getText().toString());
                                    model.put("Reg_City", edt_carReg.getText().toString());
                                    model.put("Mileage", edt_mileage.getText().toString());
                                    model.put("Color", edt_bodycolor.getText().toString());

                                    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Data").child(pushid);
                                    databaseReference.setValue(model);
                                    emptyEdtTxt();
                                    progressDialog.hide();
                                }
                            }
                        });

                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        progressDialog.setMessage("Uploading Images & Data...");
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("FirebaseFailure", "onFailure: " + e.getMessage());
                        progressDialog.hide();
                    }
                });
            }
        } else {
            Toast.makeText(this, "Plz Select Or Capture 10 Images", Toast.LENGTH_SHORT).show();
            progressDialog.hide();
        }
    }

    public boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialog("External storage", MainActivity.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat.requestPermissions(
                            (Activity) context,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }
    }

    public void showDialog(final String msg, final Context context,
                           final String permission) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage(msg + " permission is necessary");
        alertBuilder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions((Activity) context,
                                new String[]{permission},
                                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    private boolean isEmpty() {
        if (edt_bodycolor.getText().toString().equals("")) {
            return true;
        } else if (edt_carReg.getText().toString().equals("")) {
            return true;
        } else if (edt_carInfo.getText().toString().equals("")) {
            return true;
        } else if (edt_Location.getText().toString().equals("")) {
            return true;
        } else if (edt_mileage.getText().toString().equals("")) {
            return true;
        } else {
            return false;
        }
    }

    private void emptyEdtTxt() {
        edt_mileage.setText("");
        edt_Location.setText("");
        edt_carInfo.setText("");
        edt_carReg.setText("");
        edt_bodycolor.setText("");
        imagesList = new ArrayList<>();
        adapter = new PhotosAdapter(imagesList, this);
        mCardView_upload.setVisibility(View.VISIBLE);
        mCardView_photos.setVisibility(View.GONE);
    }

    private void checkEmptyArray(){
        if (adapter!= null){
            adapter.setOnClearArray(new PhotosAdapter.ClearArray() {
                @Override
                public void onClearArray() {
                    mCardView_upload.setVisibility(View.VISIBLE);
                    mCardView_photos.setVisibility(View.GONE);
                }
            });
        }
    }

}
