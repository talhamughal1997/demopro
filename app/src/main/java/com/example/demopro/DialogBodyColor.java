package com.example.demopro;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

public class DialogBodyColor extends DialogFragment implements View.OnClickListener {


    View view;
    TextView txt_Cancel;
    CardView cv_white, cv_yellow, cv_fuchsia, cv_red, cv_silver, cv_gray,
            cv_olive, cv_purple, cv_maroon, cv_aqua, cv_lime, cv_green, cv_blue, cv_navy, cv_black;

    itemClick mlistener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().getWindow().setWindowAnimations(R.style.MyDialogAnimation2);

        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_bodycolor, null, false);
        viewInit();
        initListeners();

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setCancelable(false)
                .create();
    }

    private void viewInit() {
        txt_Cancel = view.findViewById(R.id.txt_cancel);
        cv_aqua = view.findViewById(R.id.cardview_aqua);
        cv_black = view.findViewById(R.id.cardview_black);
        cv_blue = view.findViewById(R.id.cardview_blue);
        cv_fuchsia = view.findViewById(R.id.cardview_fuchsia);
        cv_green = view.findViewById(R.id.cardview_green);
        cv_maroon = view.findViewById(R.id.cardview_maroon);
        cv_lime = view.findViewById(R.id.cardview_lime);
        cv_yellow = view.findViewById(R.id.cardview_yellow);
        cv_white = view.findViewById(R.id.cardview_white);
        cv_gray = view.findViewById(R.id.cardview_gray);
        cv_red = view.findViewById(R.id.cardview_red);
        cv_navy = view.findViewById(R.id.cardview_navy);
        cv_purple = view.findViewById(R.id.cardview_purple);
        cv_olive = view.findViewById(R.id.cardview_olive);
        cv_silver = view.findViewById(R.id.cardview_silver);
    }

    private void initListeners() {
        txt_Cancel.setOnClickListener(this);
        cv_aqua.setOnClickListener(this);
        cv_black.setOnClickListener(this);
        cv_blue.setOnClickListener(this);
        cv_fuchsia.setOnClickListener(this);
        cv_green.setOnClickListener(this);
        cv_maroon.setOnClickListener(this);
        cv_lime.setOnClickListener(this);
        cv_yellow.setOnClickListener(this);
        cv_white.setOnClickListener(this);
        cv_gray.setOnClickListener(this);
        cv_red.setOnClickListener(this);
        cv_navy.setOnClickListener(this);
        cv_purple.setOnClickListener(this);
        cv_olive.setOnClickListener(this);
        cv_silver.setOnClickListener(this);
    }

    public void setOnColorClickListener(itemClick listener) {
        this.mlistener = listener;
    }

    interface itemClick {
        void selectedColor(String string);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_cancel: {
                dismiss();
                break;
            }
            case R.id.cardview_aqua: {
                mlistener.selectedColor("Aqua");
                dismiss();
                break;
            }
            case R.id.cardview_black: {
                mlistener.selectedColor("Black");
                dismiss();
                break;
            }
            case R.id.cardview_blue: {
                mlistener.selectedColor("Blue");
                dismiss();
                break;
            }
            case R.id.cardview_fuchsia: {
                mlistener.selectedColor("Fuchsia");
                dismiss();
                break;
            }
            case R.id.cardview_green: {
                mlistener.selectedColor("Green");
                dismiss();
                break;
            }
            case R.id.cardview_maroon: {
                mlistener.selectedColor("Maroon");
                dismiss();
                break;
            }
            case R.id.cardview_lime: {
                mlistener.selectedColor("Lime");
                dismiss();
                break;
            }
            case R.id.cardview_yellow: {
                mlistener.selectedColor("Yellow");
                dismiss();
                break;
            }
            case R.id.cardview_white: {
                mlistener.selectedColor("White");
                dismiss();
                break;
            }
            case R.id.cardview_gray: {
                mlistener.selectedColor("Gray");
                dismiss();
                break;
            }
            case R.id.cardview_red: {
                mlistener.selectedColor("Red");
                dismiss();
                break;
            }
            case R.id.cardview_navy: {
                mlistener.selectedColor("Navy");
                dismiss();
                break;
            }
            case R.id.cardview_purple: {
                mlistener.selectedColor("Purple");
                dismiss();
                break;
            }
            case R.id.cardview_olive: {
                mlistener.selectedColor("Olive");
                dismiss();
                break;
            }
            case R.id.cardview_silver: {
                mlistener.selectedColor("Silver");
                dismiss();
                break;
            }
        }
    }
}

