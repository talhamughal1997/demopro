package com.example.demopro;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

public class DeleteDialog extends DialogFragment {
    View view;
    LinearLayout ln_delete;
    private itemClickListener mListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_edit, null, false);
        ln_delete = view.findViewById(R.id.ln_delete);
        ln_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                mListener.itemClick();
            }
        });
        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .create();
    }

    public void setOnItemClickListener(DeleteDialog.itemClickListener onButtonClick) {
        this.mListener = onButtonClick;
    }


    public interface itemClickListener {
        void itemClick();
    }
}
